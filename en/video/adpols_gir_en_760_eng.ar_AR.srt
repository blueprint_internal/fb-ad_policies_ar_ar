﻿1
00:00:03,222 --> 00:00:05,804
‫يستخدم أكثر من مليار شخص فيسبوك يوميًا

2
00:00:05,804 --> 00:00:09,062
في التواصل عن قرب مع أفراد العائلة
والأصدقاء، وكذلك مع الأنشطة التجارية

3
00:00:09,062 --> 00:00:11,421
يوفر فيسبوك مساحة خاصة للأشخاص

4
00:00:11,421 --> 00:00:12,766
ونود أن نساعد في تحويله إلى

5
00:00:12,766 --> 00:00:15,429
منصة عالية الجودة يمكن الثقة بها

6
00:00:15,429 --> 00:00:19,247
ولذلك، وضع فيسبوك سياسة
شاملة لنشر الإعلانات

7
00:00:19,247 --> 00:00:23,015
لضمان أن تلبي جميع
العمليات توقعات المستخدمين

8
00:00:23,015 --> 00:00:24,203
وبالنسبة إلى الأنشطة التجارية

9
00:00:24,203 --> 00:00:27,531
يحقق استخدام الصور والنصوص
المناسبة في الإعلانات

10
00:00:27,531 --> 00:00:29,484
فوائد جيدة

11
00:00:29,484 --> 00:00:30,509
بينما يؤدي استخدام إعلانات غير مناسبة

12
00:00:30,509 --> 00:00:33,023
إلى حدوث عكس ذلك

13
00:00:33,023 --> 00:00:35,234
دعونا نستعرض مثالاً لذلك

14
00:00:35,234 --> 00:00:38,906
يرغب هذا الموقع المتخصص في التجارة الإلكترونية
في جذب عدد أكبر من الأشخاص لزيارته

15
00:00:38,906 --> 00:00:41,132
لترويج خصومات على المنتجات

16
00:00:41,132 --> 00:00:42,429
وفي سبيل تحسين الجاذبية

17
00:00:42,429 --> 00:00:43,590
استخدموا أسماءً لعلامات تجارية أخرى مشهورة

18
00:00:43,590 --> 00:00:46,277
على نطاق الويب الخاص بهم

19
00:00:46,277 --> 00:00:49,398
وأفرطوا في عمليات الترويج

20
00:00:49,398 --> 00:00:52,585
وقاموا أيضًا بإضافة شعارات
لمختلف العلامات التجارية للحقائب

21
00:00:52,585 --> 00:00:56,225
واعتقدوا أن القيام بذلك
سيجعل الإعلانات أكثر جاذبية

22
00:00:56,225 --> 00:00:59,187
لكن جميع ما قاموا به أتى بنتائج عكسية

23
00:00:59,187 --> 00:01:00,180
فهم لم ينتهكوا

24
00:01:00,180 --> 00:01:03,125
سياسة نشر الإعلانات فيسبوك وحسب

25
00:01:03,125 --> 00:01:06,399
بل فقدوا أيضًا ثقة الأشخاص الذين
ظهرت لهم الإعلانات في منتجاتهم

26
00:01:06,399 --> 00:01:08,497
وفاقت الخسارة ما تحقق من مكاسب

27
00:01:08,497 --> 00:01:09,415
وعلى نحو مماثل

28
00:01:09,415 --> 00:01:11,875
إذا كنت مطور تطبيقات

29
00:01:11,875 --> 00:01:13,127
يرجى التأكد من وصف

30
00:01:13,127 --> 00:01:15,898
الوظائف الفعلية للتطبيق في الإعلان

31
00:01:15,898 --> 00:01:18,492
لتجنب المقارنات غير الواقعية
'لما قبل الاستخدام وما بعده'

32
00:01:18,492 --> 00:01:21,382
أو عدم إمكانية توفير
التأثيرات والوظائف المتوقعة

33
00:01:21,382 --> 00:01:23,304
وبسبب هذه الممارسات غير الجيدة

34
00:01:23,304 --> 00:01:26,781
تُفقد الثقة المتبادلة بينك وبين المستخدمين

35
00:01:26,781 --> 00:01:27,924
دعونا نستعرض مثالاً آخر

36
00:01:27,924 --> 00:01:30,085
لمتجر على الإنترنت متخصص في بيع
ملابس السباحة والملابس الداخلية

37
00:01:30,085 --> 00:01:32,585
استخدموا صور هذه المنتجات في إعلاناتهم

38
00:01:32,585 --> 00:01:35,218
لكن الإعلانات لم تترك انطباعًا
سيئًا لدى الجمهور وحسب

39
00:01:35,218 --> 00:01:38,718
بل انتهكت أيضًا سياسة نشر
الإعلانات على فيسبوك

40
00:01:39,976 --> 00:01:41,632
وبصرف النظر عن استخدام صور تحتوي
على إغراءات جنسية بشكل مفرط

41
00:01:41,632 --> 00:01:44,398
لا يمكن أن تكون الإعلانات صادمة

42
00:01:44,398 --> 00:01:45,391
أو تحتوى على مشاهد عنيفة

43
00:01:45,391 --> 00:01:47,781
أو تتضمن محتوى عديم الاحترام

44
00:01:47,781 --> 00:01:50,398
ولاسيما في تطبيقات الألعاب

45
00:01:50,398 --> 00:01:51,406
وتتمثل أفضل ممارسة في

46
00:01:51,406 --> 00:01:54,929
التركيز على التعريف بمزايا
المنتج والميزات الفريدة له

47
00:01:54,929 --> 00:01:57,796
ويُحظر تمامًا استخدام

48
00:01:57,796 --> 00:02:00,070
عبارات مُبالغ فيها أو زائفة لتضليل المشترين

49
00:02:01,453 --> 00:02:03,031
هناك العديد من الطرق

50
00:02:03,031 --> 00:02:04,304
على فيسبوك

51
00:02:04,304 --> 00:02:06,695
للإعلان عن المنتجات والخدمات التي تقدمها

52
00:02:06,695 --> 00:02:08,632
وطالما كنت مبدعًا عند
اختيار المحتوى المناسب

53
00:02:08,632 --> 00:02:10,468
وإنشاء إعلانات عالية الجودة

54
00:02:10,468 --> 00:02:11,531
أصبح بإمكانك

55
00:02:11,531 --> 00:02:13,507
ترسيخ صورة إيجابية للعلامة التجارية

56
00:02:13,507 --> 00:02:15,445
وتأسيس سمعة جيدة ودائمة

57
00:02:15,445 --> 00:02:17,414
وزيادة الثقة لدى عملائك

58
00:02:17,414 --> 00:02:20,335
وزيادة نقاط ملاءمة الإعلانات على فيسبوك

59
00:02:20,335 --> 00:02:24,125
والحصول على فرص أكبر للفوز بعروض
الأسعار وتقديم عروض أسعار أقل

60
00:02:24,125 --> 00:02:26,359
وتأسيس دورة سليمة لترويج الإعلانات

61
00:02:26,359 --> 00:02:27,656
الإعلان الجيد

62
00:02:27,656 --> 00:02:30,484
هو الإعلان الذي يجذب
الأشخاص أثناء تمرير الشاشة

63
00:02:30,484 --> 00:02:31,695
ويلامس شغاف قلوبهم

64
00:02:31,695 --> 00:02:34,085
وهذا من شأنه المساعدة في
جذب المزيد من فرص الأعمال

65
00:02:34,085 --> 00:02:36,125
والتعامل بأمانة وصدق

66
00:02:36,125 --> 00:02:37,968
ويمكن من خلال مشاركة قصة علامتك التجارية

67
00:02:37,968 --> 00:02:40,664
جذب الأشخاص لحبها

68
00:02:42,867 --> 00:02:43,813
وذلك لأنه

69
00:02:43,813 --> 00:02:46,308
عند القيام بذلك بشكل
صحيح، تتحقق أفضل النتائج

